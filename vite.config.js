import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react';
import yaml from '@rollup/plugin-yaml';
import reactScopedCssPlugin from 'rollup-plugin-react-scoped-css';
const path = require("path");

export default defineConfig({
  plugins: [
    yaml(),
    react(),
    reactScopedCssPlugin(),
  ],
  resolve: {
    alias: {
      'src': path.resolve('src/'),
      'components': path.resolve('src/components/'),
      'assets': path.resolve('src/assets/'),
      'hooks': path.resolve('src/hooks/'),
      'services': path.resolve('src/services/'),
      'utils': path.resolve('src/utils/'),
      'store': path.resolve('src/store/'),
    }
  }
})
