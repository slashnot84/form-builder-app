module.exports = {
    "roots": [
        "<rootDir>/src"
    ],
    "setupFilesAfterEnv": [
        "<rootDir>/src/setupTests.js"
    ],
    "collectCoverageFrom": [
        "src/**/*.{js,jsx,ts,tsx}",
        "!src/**/*.d.ts"
    ],
    "testMatch": [
        "<rootDir>/src/**/__tests__/**/*.{js,jsx,ts,tsx}",
        "<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}"
        // '**/__tests__/**/*.[jt]s?(x)',
        // '**/?(*.)+(spec|test).[jt]s?(x)',
        // '**/?(*.)+(specs|tests).[jt]s?(x)'
    ],
    "testEnvironment": "jsdom",
    "transform": {
        "^.+\\.(js|jsx|mjs|cjs|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
        "^.+\\.scss$": "jest-scss-transform",
        "^.+\\.(yaml|yml)$": "jest-transform-yaml",
    },
    "transformIgnorePatterns": [
        "[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs|cjs|ts|tsx)$",
        "^.+\\.module\\.(css|sass|scss)$"
    ],
    "modulePaths": [
        "<rootDir>"
    ],
    "moduleDirectories": [
        "node_modules",
        "src"
    ],
    "moduleNameMapper": {
        "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy",
        "^components(.*)$": "<rootDir>/src/components$1",
        "^hooks(.*)$": "<rootDir>/src/hooks$1",
        "^services(.*)$": "<rootDir>/src/services$1",
        "^App(.*)$": "<rootDir>/src$1"
    },
    "moduleFileExtensions": ['js', 'jsx', 'ts', 'tsx', 'json', 'node', 'yaml', 'yml'],
    "watchPlugins": [
        "jest-watch-typeahead/filename",
        "jest-watch-typeahead/testname"
    ],
    "resetMocks": true
}