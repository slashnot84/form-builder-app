import '@testing-library/jest-dom';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

Object.defineProperty(window, 'matchMedia', {
    value: () => {
      return {
        matches: false,
        addListener: () => {},
        removeListener: () => {}
      };
    }
  });
  
  Object.defineProperty(window, 'getComputedStyle', {
    value: () => {
      return {
        getPropertyValue: () => {}
      };
    }
  });