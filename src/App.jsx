import FormEditor from 'components/FormEditor/FormEditor'
import './App.scss'

function App() {

  return (
    <div className="App">
      <FormEditor></FormEditor>
    </div>
  )
}

export default App
