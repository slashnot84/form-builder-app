import { Editor, Frame, Element, useEditor, useNode } from "@craftjs/core";
import Toolbox from "./Toolbox";
import SettingsPanel from "./SettingsPanel";
import * as formItems from './DragItems';
import { Button } from 'antd';
import TopBar from "./TopBar";
import './FormEditor.scss';

const FormEditor = () => {
    const json = JSON.parse(localStorage.getItem('form-builder'));

    return (
        <div className="form-editor">
            <Editor resolver={{ ...formItems, Button }}>
                <TopBar></TopBar>
                <div className="editor">
                    <Toolbox></Toolbox>
                    <div className="canvas">
                        <Frame json={json}>
                            <Element is={formItems.FormContainer} canvas>
                                {/* Items will be dropped here */}
                            </Element>
                        </Frame>
                    </div>
                    <SettingsPanel></SettingsPanel>
                </div>
            </Editor>
        </div>
    );
};

export default FormEditor;