import React from "react";
import { useEditor } from "@craftjs/core";
import { Button, Card, Tabs } from 'antd';

const { TabPane } = Tabs;
const SettingsPanel = () => {
    const { selected, actions } = useEditor((state) => {
        const currentNodeId = state.events.selected;
        const currentId = Array.from(currentNodeId)[0];
        let selected;

        if (currentNodeId) {
            selected = {
                id: currentId,
                name: state.nodes[currentId]?.data?.name,
                settings: state.nodes[currentId]?.related && state.nodes[currentId]?.related?.settings,
                validations: state.nodes[currentId]?.related && state.nodes[currentId]?.related?.validations,
                label: state.nodes[currentId]?.data.displayName || state.nodes[currentId]?.data?.name,
            };
        }

        return {
            selected
        }
    });

    return (
        <div className="settings-panel">
            {selected.settings ? (
                <>
                    <Card>
                        <Tabs defaultActiveKey="properties">
                            <TabPane tab="Properties" key="properties">
                                {selected.settings && React.createElement(selected.settings)}
                            </TabPane>
                            <TabPane tab="Validations" key="validations">
                                {selected.validations && React.createElement(selected.validations)}
                            </TabPane>
                        </Tabs>
                    </Card>

                    <div className="settings-card-actions">
                        <Button onClick={() => actions.delete(selected.id)} type="default" shape="round">Delete</Button>
                    </div>
                </>
            )
                : (
                    selected.name && <h4>No settings for {selected.name}</h4>
                )}

        </div>
    );
};

export default SettingsPanel;