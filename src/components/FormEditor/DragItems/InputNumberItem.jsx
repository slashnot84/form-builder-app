import React from 'react';
import { useNode } from "@craftjs/core";
import { Form, Input, InputNumber, Select, Switch } from 'antd';
import { Validations, InputSettings } from './InputItemSettings';
import { nanoid } from 'nanoid';

const InputNumberItem = (props) => {
    const { name, defaultValue, placeHolder, label } = props;
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`form-item-container ${selected && 'selected'}`}>
            <Form.Item initialValue={defaultValue} name={name} label={label}>
                <InputNumber min={props.min} max={props.max} step={props.step} placeholder={placeHolder} />
            </Form.Item>
        </div>
    );
};

// ----------------------------------------------------------------------------

export const DefaultProps = {
    id: nanoid(10),
    defaultValue: '',
    placeHolder: '',
    label: 'New Label',
    min: 0,
    max: 100,
    step: 1
};

InputNumberItem.craft = {
    props: DefaultProps,
    displayName: 'Number',
    related: {
        settings: InputSettings,
        validations: Validations
    },
};

export default InputNumberItem;