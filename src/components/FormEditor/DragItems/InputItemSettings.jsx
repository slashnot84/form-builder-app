import React from 'react';
import { useNode } from "@craftjs/core";
import { Form, Input, InputNumber, Select, Switch, Button, Divider, Space, Row, Col } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { nanoid } from 'nanoid';

const formItemLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

export const InputSettings = () => {
    const { Option } = Select;

    const { actions: { setProp }, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            Object.keys(vals).map(key => {
                props[key] = vals[key]
            });
        }, 1000)
    }

    return (
        <Form labelAlign="left" {...formItemLayout} initialValues={collected} onValuesChange={handleFormChange} className="text-field-settings">
            <Form.Item label="Label" name="label" rules={[{ required: true, message: 'Please enter a label' }]}>
                <Input />
            </Form.Item>

            <Form.Item label="Name" name="name">
                <Input />
            </Form.Item>

            <Form.Item label="Placeholder" name="placeHolder">
                <Input />
            </Form.Item>

            <Form.Item label="Default Value" name="defaultValue">
                <Input />
            </Form.Item>

            {collected.min !== undefined && (
                <Form.Item label="Min Value" name="min">
                    <InputNumber />
                </Form.Item>
            )}

            {collected.max && (
                <Form.Item label="Max Value" name="max">
                    <InputNumber />
                </Form.Item>
            )}

            {collected.step && (
                <Form.Item label="Step" name="step">
                    <InputNumber />
                </Form.Item>
            )}

            {collected.showCount !== undefined && (
                <Form.Item label="Show Count" name="showCount">
                    <Switch defaultChecked={collected.showCount} size="small" />
                </Form.Item>
            )}

            {collected.type && (
                <Form.Item label="Input type" name="type" initialValue="text">
                    <Select>
                        <Option value="text">Text</Option>
                        <Option value="search">Search</Option>
                        <Option value="email">Email</Option>
                    </Select>
                </Form.Item>
            )}


            {collected.choices && (
                <>
                    <Divider type="horizontal" />
                    <h3>Options</h3>
                    <Form.List name="choices">
                        {(fields, { add, remove }, { errors }) => (
                            <>
                                {fields.map(({ key, name, fieldKey, ...restField }) => (
                                    <Row key={key} gutter={5} align="top" justify="space-between">
                                        <Col span={1}><h5>{key}: </h5></Col>
                                        <Col span={10}>
                                            <Form.Item
                                                {...restField}
                                                wrapperCol={10}
                                                name={[name, 'label']}
                                                style={{ flexBasis: '100%', flexGrow: 2 }}
                                                fieldKey={[fieldKey, 'label']}>
                                                <Input placeholder="label" />
                                            </Form.Item>
                                        </Col>
                                        <Col span={10}>
                                            <Form.Item {...restField}
                                                wrapperCol={10}
                                                name={[name, 'value']}
                                                style={{ flexBasis: '100%', flexGrow: 2 }}
                                                fieldKey={[fieldKey, 'value']}>
                                                <Input placeholder="value" />
                                            </Form.Item>
                                        </Col>
                                        <Col span={2}><MinusCircleOutlined onClick={() => remove(name)} /></Col>
                                    </Row>
                                ))}
                                <div style={{ width: '100%', textAlign: 'center' }}>
                                    <Button
                                        type="dashed"
                                        onClick={() => add()}
                                        icon={<PlusOutlined />}
                                    >
                                        Add
                                    </Button>
                                </div>
                            </>
                        )}
                    </Form.List>
                </>
            )}
        </Form>
    );
};

export const Validations = () => {
    const { actions: { setProp }, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            Object.keys(vals).map(key => {
                props[key] = vals[key]
            });
        }, 1000)
    }

    return (
        <Form onValuesChange={handleFormChange} name="validationForm" layout="horizontal">
            <Form.Item name="validations">
                <Form.Item name="required" label="Is Required">
                    <Switch defaultChecked={collected.required} size="small" />
                </Form.Item>
            </Form.Item>
        </Form>
    )
}