import React from 'react';
import { useNode } from "@craftjs/core";
import { Slider } from 'antd'

const FormItem = ({ children, text, fontSize }) => {
    const { connectors: { connect, drag } } = useNode();
    return (
        <div ref={ref => connect(drag(ref))} className="form-item">
            {children}
        </div>
    );
};

const ItemSettings = () => {
    const {
        actions: { setProp },
        fontSize,
    } = useNode((node) => ({
        text: node.data?.props?.text,
        fontSize: node.data?.props?.fontSize,
    }));

    return (
        <div className="settings-field">
            <Slider
                value={fontSize || 7}
                step={7}
                min={1}
                max={50}
                onChange={(_, value) => {
                    setProp((props) => (props.fontSize = value), 1000);
                }}
            />
        </div>
    );
};

export const DefaultProps = {
    text: 'Hi',
    fontSize: 20,
  };

FormItem.craft = {
    props: DefaultProps,
    related: {
      settings: ItemSettings,
    },
  };

export default FormItem;