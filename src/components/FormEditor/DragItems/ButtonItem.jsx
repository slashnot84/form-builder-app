import React, { useEffect } from 'react';
import { useNode } from "@craftjs/core";
import { Button, Radio, Form, Select, Input } from 'antd';
import { nanoid } from 'nanoid';

const ButtonItem = (props) => {
    const { label, shape, type, size } = props;
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`form-item-button ${selected && 'selected'}`}>
            <Button shape={shape} size={size} type={type}>{label}</Button>
        </div>
    );
};

// ----------------------------------------------------------------------------
const { Option } = Select
const Settings = () => {
    const { actions: { setProp }, selected, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            Object.keys(vals).map(key => {
                props[key] = vals[key]
            });
        }, 1000)
    }

    return (
        <Form layout="horizontal" labelAlign="left" initialValues={collected} onValuesChange={handleFormChange}
            className={`fieldset-settings ${selected && 'selected'}`}>
            <Form.Item label="Label" name="label">
                <Input />
            </Form.Item>
            <Form.Item label="Type" name="type">
                <Select>
                    <Option value="default">Default</Option>
                    <Option value="primary">Primary</Option>
                    <Option value="dashed">Dashed</Option>
                    <Option value="link">Link</Option>
                    <Option value="text">Text</Option>
                    <Option value="ghost">Ghost</Option>
                </Select>
            </Form.Item>

            <Form.Item label="Size" name="size">
                <Radio.Group>
                    <Radio value="large">Large</Radio>
                    <Radio value="middle">Middle</Radio>
                    <Radio value="small">Small</Radio>
                </Radio.Group>
            </Form.Item>

            <Form.Item label="Shape" name="shape">
                <Radio.Group>
                    <Radio value="default">Default</Radio>
                    <Radio value="circle">Circle</Radio>
                    <Radio value="round">Round</Radio>
                </Radio.Group>
            </Form.Item>
        </Form>
    );
}

const DefaultProps = {
    id: nanoid(10),
    defaultValue: '',
    label: 'Button',
    size: 'middle',
    type: 'default',
    shape: 'default'
};

ButtonItem.craft = {
    props: DefaultProps,
    displayName: 'Button',
    related: {
        settings: Settings
    },
};

export default ButtonItem;