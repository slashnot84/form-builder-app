import React from 'react';
import { useNode, Element } from "@craftjs/core";
import { Container } from 'components/FormEditor/DragItems'
import { Form, Input, Card, Switch } from 'antd';

const FieldSet = (props) => {
    const { shadow, label } = props;
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`field-set ${selected && 'selected'} ${shadow && 'shadow'}`}>
            <Card size="small" title={label} bordered={false}>
                <Element is={Container} id="fieldset" className="fieldset-group" canvas></Element>
            </Card>
        </div>
    );
};
// ----------------------------------------------------------------------------

const Settings = () => {
    const { actions: { setProp }, id, selected, shadow, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            props.label = vals.label
            props.shadow = vals.shadow
        }, 1000)
    }

    return (
        <Form layout="horizontal" labelAlign="left" initialValues={collected} onValuesChange={handleFormChange}
            className={`fieldset-settings ${selected && 'selected'}`}>
            <Form.Item label="Label" name="label">
                <Input />
            </Form.Item>

            <Form.Item label="Drop Shadow" name="shadow">
                <Switch defaultChecked={shadow} size="small" />
            </Form.Item>
        </Form>
    );
}

export const DefaultProps = {
    label: 'Form Group',
    shadow: false
};

FieldSet.craft = {
    props: DefaultProps,
    displayName: 'Form Group',
    related: {
        settings: Settings,
    },
};

export default FieldSet;