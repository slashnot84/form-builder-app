import { useNode } from "@craftjs/core";

const ColumnItem = ({ children, span, offset }) => {
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });
    return (
        <div ref={ref => connect(drag(ref))} className={`form-column ${selected && 'selected'}`}>
            {children}
        </div>
    );
};

export default ColumnItem;