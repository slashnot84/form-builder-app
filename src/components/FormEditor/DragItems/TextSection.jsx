import { useEffect, useState } from "react";
import { useNode } from "@craftjs/core";
import ContentEditable from 'react-contenteditable'

const TextSection = ({ text }) => {
    const { connectors: { connect, drag }, selected, actions: { setProp } } = useNode((node) => {
        return {
            selected: node.events.selected,
            dragged: node.events.dragged
        }
    });
    const [editable, setEditable] = useState(false);
    useEffect(() => {
        if (!selected)
            setEditable(false);
    }, [selected]);

    return (
        <div ref={ref => connect(drag(ref))} className={`form-text-section ${selected && 'selected'}`}>
            <ContentEditable
                html={text}
                disabled={!editable}
                onDoubleClick={() => setEditable(true)}
                onChange={e =>
                    setProp(props =>
                        props.text = e.target.value
                    )
                }
                className="section-text"
                tagName="section"
            />
        </div>
    );
};

export const DefaultProps = {
    text: `
    <h2>Section Title</h2>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab totam temporibus, suscipit</p>
    `
};

TextSection.craft = {
    props: DefaultProps,
    displayName: 'Text Section',
};

export default TextSection;