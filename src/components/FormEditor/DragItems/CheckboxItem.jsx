import React, { useEffect } from 'react';
import { useNode } from "@craftjs/core";
import { Form, Checkbox } from 'antd';
import { Validations, InputSettings } from './InputItemSettings';
import { nanoid } from 'nanoid';

const CheckboxItem = (props) => {
    const { type = "text", name, defaultValue, choices = [], label } = props;
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`form-item-container ${selected && 'selected'}`}>
            <Form.Item initialValue={defaultValue} name={name}>
                <h4>{label}</h4>
                <Checkbox.Group className="form-checkbox-group">
                    {choices.map((item, i) => (
                        <Checkbox className="form-checkbox" key={i} value={item?.value}>{item?.label}</Checkbox>
                    ))}
                </Checkbox.Group>
            </Form.Item>
        </div>
    );
};

// ----------------------------------------------------------------------------

const DefaultProps = {
    id: nanoid(10),
    defaultValue: '',
    placeHolder: '',
    label: 'Checkbox Group',
    choices: [{ label: 'A', value: 'a' }, { label: 'B', value: 'b' }, { label: 'C', value: 'c' }],
    formSchema: {}
};

CheckboxItem.craft = {
    props: DefaultProps,
    displayName: 'Checkbox',
    related: {
        settings: InputSettings,
        validations: Validations
    },
};

export default CheckboxItem;