import { useNode, Element, useEditor } from "@craftjs/core";
import { Form, Input, InputNumber, Select, Button, Divider } from 'antd';
import { Container } from 'components/FormEditor/DragItems';
import { useState } from "react";
import { useHotkeys } from 'react-hotkeys-hook'


const FormContainer = ({ label, layout, padding, children }) => {
    const { connectors: { connect, drag } } = useNode();
    const [selectedNode, setSelectedNode] = useState(null);

    const { selected, actions } = useEditor((state, query) => {
        const currentNodeId = state.events.selected;
        const currentId = Array.from(currentNodeId)[0];
        let selected;

        if (currentNodeId.size) {
            selected = {
                id: currentId,
                name: state.nodes[currentId]?.data?.name,
                isDeletable: query.node(currentId).isDeletable()
            };
            setSelectedNode(selected)
        }
        return { selected }
    });

    // USE Hot Keys
    useHotkeys('Delete', () => {
        if (selected.isDeletable && selected.id) {
            actions.delete(selected.id)
        }
    }, [selected]);

    useHotkeys('ctrl+z', () => {
        actions.history.undo();
    });

    useHotkeys('ctrl+y', () => {
        actions.history.redo();
    });

    return (
        <div className="form-container">
            <Form labelAlign="left" layout={layout} className="form-content">
                <h2 className="form-title">{label}</h2>
                <Divider style={{ margin: '1em 0 0' }} />
                <div style={{ padding: `${padding - 10}px ${padding}px ${padding}px` }} className="form-fields" ref={ref => connect(drag(ref))} >
                    {children}
                </div>
                <div className="form-footer">
                    <Element id="form-footer" is={Container} canvas>
                        <Button shape="round">Cancel</Button>
                        <Button type="primary" shape="round">Submit</Button>
                    </Element>
                </div>
            </Form>
        </div>
    );
};

const Settings = () => {
    const { actions: { setProp }, id, selected, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            props.label = vals.label
            props.padding = vals.padding
            props.layout = vals.layout
        }, 1000)
    }

    const { Option } = Select;
    return (
        <Form layout="vertical" initialValues={collected} onValuesChange={handleFormChange} className={`form-settings ${selected && 'selected'}`}>
            <Form.Item label="Label" name="label">
                <Input />
            </Form.Item>

            <Form.Item label="Padding" name="padding">
                <InputNumber />
            </Form.Item>

            <Form.Item label="Form Layout" name="layout">
                <Select>
                    <Option value="horizontal">Horizontal</Option>
                    <Option value="vertical">Vertical</Option>
                    <Option value="inline">Inline</Option>
                </Select>
            </Form.Item>
        </Form>
    );
}

export const DefaultProps = {
    label: 'Form',
    padding: 40,
    layout: 'horizontal'
};

FormContainer.craft = {
    props: DefaultProps,
    displayName: 'Form',
    related: {
        settings: Settings,
    },
};

export default FormContainer;