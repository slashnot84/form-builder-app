import React, { useEffect } from 'react';
import { useNode } from "@craftjs/core";
import { Form, Input, Select, Switch } from 'antd';
import { Validations, InputSettings } from './InputItemSettings';
import { nanoid } from 'nanoid';

const InputItem = (props) => {
    const { type = "text", name, defaultValue, placeHolder, label } = props;
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`form-item-container ${selected && 'selected'}`}>
            <Form.Item initialValue={defaultValue} name={name} label={label}>
                <Input type={type} placeholder={placeHolder} />
            </Form.Item>
        </div>
    );
};

// ----------------------------------------------------------------------------

const DefaultProps = {
    id: nanoid(10),
    defaultValue: '',
    placeHolder: '',
    label: 'New Label',
    formSchema: {},
    type: 'text'
};

InputItem.craft = {
    props: DefaultProps,
    displayName: 'Textbox',
    related: {
        settings: InputSettings,
        validations: Validations
    },
};

export default InputItem;