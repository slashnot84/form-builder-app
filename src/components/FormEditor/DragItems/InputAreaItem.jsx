import React, { useEffect } from 'react';
import { useNode } from "@craftjs/core";
import { Form, Input, Select, Switch } from 'antd';
import { Validations, InputSettings } from './InputItemSettings';
import { nanoid } from 'nanoid';

const { TextArea } = Input;
const InputAreaItem = (props) => {
    const { type = "text", name, defaultValue, placeHolder, label } = props;
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`form-item-container ${selected && 'selected'}`}>
            <Form.Item initialValue={defaultValue} name={name} label={label}>
                <TextArea showCount={props.showCount} maxLength={props.max} />
            </Form.Item>
        </div>
    );
};

// ----------------------------------------------------------------------------

const DefaultProps = {
    id: nanoid(10),
    defaultValue: '',
    placeHolder: '',
    label: 'New Label',
    max: 100,
    showCount: true,
    formSchema: {}
};

InputAreaItem.craft = {
    props: DefaultProps,
    displayName: 'TextArea',
    related: {
        settings: InputSettings,
        validations: Validations
    },
};

export default InputAreaItem;