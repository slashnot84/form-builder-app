import { useNode, Element } from "@craftjs/core";
import ColumnItem from './ColumnItem';
import { Slider, Form, Input, InputNumber, Select } from "antd";

const GridItem = ({ gutter, gutterUnit, cols }) => {
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });
    const colsArr = new Array(cols).fill(true)
    return (
        <div ref={ref => connect(drag(ref))} style={{ '--gutter': `${gutter}${gutterUnit}` }} className={`form-grid ${selected && 'selected'}`}>
            {colsArr.map((col, i) => (
                <Element key={i} id={`form-elm-col-${i}`} is={ColumnItem} canvas>

                </Element>
            ))}
        </div>
    );
};
// ----------------------------------------------------------------------------

const UnitDropDown = (
    <Form.Item name="gutterUnit" initialValue="px" noStyle>
        <Select>
            <Select.Option value="px">px</Select.Option>
            <Select.Option value="em">em</Select.Option>
            <Select.Option value="%">%</Select.Option>
        </Select>
    </Form.Item>
);

const marks = {
    1: '1',
    2: '2',
    3: '3',
    4: '4'
}

const Settings = () => {
    const { actions: { setProp }, id, selected, shadow, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            Object.keys(vals).map(key => {
                props[key] = vals[key]
            });
        }, 1000)
    }

    return (
        <Form layout="horizontal" labelAlign="left" initialValues={collected} onValuesChange={handleFormChange}
            className={`grid-item-settings ${selected && 'selected'}`}>
            <Form.Item label="Label" name="label">
                <Input />
            </Form.Item>

            <Form.Item style={{ padding: '0 0.5em' }} labelCol={{ span: 24 }} label="Columns" name="cols">
                <Slider marks={marks} min={1} max={4} step={1} />
            </Form.Item>

            <Form.Item label="Gap" name="gutter">
                <InputNumber min={1} max={24} step={1} addonAfter={UnitDropDown} style={{ width: '100%' }} />
            </Form.Item>
        </Form>
    );
}

export const DefaultProps = {
    cols: 2,
    gutter: 1.5,
    gutterUnit: 'em',
    label: 'Grid'
};

GridItem.craft = {
    props: DefaultProps,
    displayName: 'Grid',
    related: {
        settings: Settings,
    },
};
export default GridItem;