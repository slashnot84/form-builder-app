import { AlignCenterOutlined, AlignLeftOutlined, AlignRightOutlined } from "@ant-design/icons";
import { useNode } from "@craftjs/core";
import { Divider, Radio, Form, Input, Slider } from "antd";

const DividerItem = ({ orientation, text, heading, spacing }) => {
    const { connectors: { connect, drag }, selected } = useNode((node) => {
        return {
            selected: node.events.selected
        }
    });

    return (
        <div ref={ref => connect(drag(ref))} className={`form-divider ${selected && 'selected'}`}>
            <Divider orientation={orientation} style={{ padding: `${spacing}px 0`, margin: `10px 0` }}>
                {heading === 1 && <h1>{text}</h1>}
                {heading === 2 && <h2>{text}</h2>}
                {heading === 3 && <h3>{text}</h3>}
                {heading === 4 && <h4>{text}</h4>}
                {heading === 5 && <h5>{text}</h5>}
            </Divider>
        </div>
    );
};
// ----------------------------------------------------------------------------

const marks = {
    1: 'h1',
    2: 'h2',
    3: 'h3',
    4: 'h4',
    5: 'h5',
}
const Settings = () => {
    const { actions: { setProp }, id, selected, shadow, ...collected } = useNode((node) => {
        return {
            ...node.data.props
        }
    });

    const handleFormChange = (_, vals) => {
        setProp(props => {
            Object.keys(vals).map(key => {
                props[key] = vals[key]
            });
        }, 1000)
    }

    return (
        <Form layout="horizontal" labelAlign="left" initialValues={collected} onValuesChange={handleFormChange}
            className={`grid-item-settings ${selected && 'selected'}`}>
            <Form.Item label="Text" name="text">
                <Input />
            </Form.Item>

            <Form.Item labelCol={{ span: 24 }} label="Alignment" name="orientation">
                <Radio.Group>
                    <Radio.Button value="left"><AlignLeftOutlined /></Radio.Button>
                    <Radio.Button value="center"><AlignCenterOutlined /></Radio.Button>
                    <Radio.Button value="right"><AlignRightOutlined /></Radio.Button>
                </Radio.Group>
            </Form.Item>

            <Form.Item style={{ padding: '0 0.5em' }} labelCol={{ span: 24 }} label="Heading" name="heading">
                <Slider marks={marks} min={1} max={5} step={1} />
            </Form.Item>

            <Form.Item style={{ padding: '0 0.5em' }} labelCol={{ span: 24 }} label="Spacing" name="spacing">
                <Slider min={0} max={60} step={1} />
            </Form.Item>
        </Form>
    );
}

export const DefaultProps = {
    text: 'Divider Text',
    orientation: 'left',
    heading: 4,
    spacing: 10
};

DividerItem.craft = {
    props: DefaultProps,
    displayName: 'Grid',
    related: {
        settings: Settings,
    },
};
export default DividerItem;