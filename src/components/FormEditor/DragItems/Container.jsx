import { useNode } from "@craftjs/core";

const Container = ({ background, padding = 0, children }) => {
    const { connectors: { connect, drag } } = useNode();
    return (
        <div ref={ref => connect(drag(ref))} className="container">
            {children}
        </div>
    );
};

export default Container;