import React from 'react';
import { useEditor } from "@craftjs/core";
import { Button as AntdButton } from 'antd';
import { FormOutlined, FontSizeOutlined, GroupOutlined } from '@ant-design/icons';
import { FormItem, InputItem, FieldSet } from './DragItems';
import toolsSchema from './ToolboxSchema';
import './Toolbox.scss';

const Toolbox = () => {
    const { connectors, query } = useEditor();
    return (
        <div className="toolbox">
            {toolsSchema.map((item, i) => {
                const { component: Component, title, ...props } = item
                return (
                    <AntdButton key={i} {...props} className="component-btn" ref={ref => connectors.create(ref,
                        <Component></Component>
                    )}>
                        {title}
                    </AntdButton>
                )
            })}
        </div>
    );
};

export default Toolbox;