import { DashOutlined,FormOutlined, FieldBinaryOutlined, FontSizeOutlined, GroupOutlined, CheckCircleOutlined, CheckSquareOutlined, BarsOutlined, BlockOutlined, MenuOutlined, BorderOutlined } from '@ant-design/icons';
import { FormItem, InputItem, FieldSet, InputNumberItem, InputAreaItem, CheckboxItem, RadioItem, SelectItem, GridItem, TextSection, DividerItem, ButtonItem } from './DragItems'

const toolsSchema = [
    {
        id: '',
        name: 'textInput',
        title: 'Text',
        component: InputItem,
        shape: 'round',
        icon: <FontSizeOutlined />
    },
    {
        id: '',
        name: 'numberInput',
        title: 'Number',
        component: InputNumberItem,
        shape: 'round',
        icon: <FieldBinaryOutlined />
    },
    {
        id: '',
        name: 'areaInput',
        title: 'Text Area',
        component: InputAreaItem,
        shape: 'round',
        icon: <GroupOutlined />
    },
    {
        id: '',
        name: 'checkboxGroup',
        title: 'Checkbox',
        component: CheckboxItem,
        shape: 'round',
        icon: <CheckSquareOutlined />
    },
    {
        id: '',
        name: 'radioGroup',
        title: 'Options',
        component: RadioItem,
        shape: 'round',
        icon: <CheckCircleOutlined />
    },
    {
        id: '',
        name: 'selectInput',
        title: 'Select',
        component: SelectItem,
        shape: 'round',
        icon: <BarsOutlined />
    },
    {
        id: '',
        name: 'button',
        title: 'Button',
        component: ButtonItem,
        shape: 'round',
        icon: <BorderOutlined />
    },
    {
        id: '',
        name: 'formGroup',
        title: 'Form Group',
        component: FieldSet,
        shape: 'round',
        icon: <BlockOutlined />
    },
    {
        id: '',
        name: 'textSection',
        title: 'Text Section',
        component: TextSection,
        shape: 'round',
        icon: <MenuOutlined />
    },
    {
        id: '',
        name: 'divider',
        title: 'Divider',
        component: DividerItem,
        shape: 'round',
        icon: <DashOutlined />
    },
    {
        id: '',
        name: 'grid',
        title: 'Grid',
        component: GridItem,
        shape: 'round',
        icon: <FormOutlined />
    }
]

export default toolsSchema