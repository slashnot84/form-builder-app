import React from 'react';
import { useEditor } from "@craftjs/core";
import { Button } from 'antd';
import { SaveOutlined, CodeOutlined } from '@ant-design/icons';

const TopBar = () => {
    const { actions, query, enabled } = useEditor((state) => ({
        enabled: state.options.enabled
    }));

    const saveLayout = () => {
        console.log(query.serialize())
        localStorage.setItem('form-builder', query.serialize());
    }
    return (
        <header className="header">
            <div className="logo"></div>
            <div className="rt-buttons">
                <Button shape="circle" icon={<CodeOutlined />} onClick={saveLayout}></Button>
                <Button shape="circle" icon={<SaveOutlined />} onClick={saveLayout}></Button>
            </div>
        </header>
    );
};

export default TopBar;